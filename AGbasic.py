#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import array
import random
import math

import numpy as np

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
def load_individuals(creator,n):
    individuals = []
    for i in range(n):
        with open("40-{0}.txt".format(i),"r") as tsp_data:
            individual = eval(tsp_data.readline())# <= load  individual from file, it returns an list thanks to 'eval' function
        individual = creator(individual)# <= transform list to Individual, Individual was defined in the previous one, and passed by parameter
        individuals.append(individual)
    return individuals
def customize_individual():
	return [1,1,0,0]
def customize_population_config(creator,toolbox):
	print("Select customize population")
	toolbox.register("population", load_individuals,  creator.Individual)
def customize_individual_config(creator,toolbox):
	# obviously we generate more elaborate individuals
	print("Select customize individual")
	# Individual generator
	toolbox.register("individual", tools.initIterate, creator.Individual, customize_individual)
	toolbox.register("population", tools.initRepeat, list, toolbox.individual)
def random_population_config(creator,toolbox):
	print("Select random population")
	# Attribute generator
	toolbox.register("attr_bool", random.randint, 0, 1)
	# Individual generator
	toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, 4)
	toolbox.register("population", tools.initRepeat, list, toolbox.individual)

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", array.array, typecode='b', fitness=creator.FitnessMax)

toolbox = base.Toolbox()

#we execute the function depends on the configuration we need
#random_population_config(creator,toolbox)
customize_population_config(creator,toolbox)
#customize_individual_config(creator,toolbox)
def evalOneMax(individual):
    seq = np.array(individual)
    strnum= "".join(map(str, seq))
    num=int(strnum,2)

    r =math.fabs((num-5)/(2+math.sin(num)))
    return r,

toolbox.register("evaluate", evalOneMax)
toolbox.register("mate", tools.cxOnePoint)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
toolbox.register("select", tools.selTournament, tournsize=3)

def main():
    random.seed(64)
    iterations=40
    pop = toolbox.population(n=7)
    hof = tools.HallOfFame(7)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    print(pop)
    pop, log = algorithms.eaSimple(pop, toolbox, cxpb=0.5, mutpb=0.2, ngen=iterations, 
                                   stats=stats, halloffame=hof, verbose=True)
        #SE GUARDA EL INDIVIDUO EN BINARIO EN UN ARCHIVO
    for i,hofi in enumerate(hof):
        name_file = "{0}-{1}.txt".format(iterations,i)
        mfile = open(name_file,"w")
        mfile.write(str(hofi.tolist()))
        fitness = "fitness: " + str(evalOneMax(hofi)  )
        print(fitness)
        
    return pop, log, hof

if __name__ == "__main__":
    main()
